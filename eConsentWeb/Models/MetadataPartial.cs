﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;

namespace eConsentWeb
{
    [MetadataType(typeof(MS_UserMetadata))]
    public partial class MS_User { }
    
    [MetadataType(typeof(MS_eConsentMetadata))]
    public partial class MS_eConsent { }

    [MetadataType(typeof(MS_eConsent_HCPProfileMetadata))]
    public partial class MS_eConsent_HCPProfile { }

    [MetadataType(typeof(TR_eConsentMetadata))]
    public partial class TR_eConsent { }
}