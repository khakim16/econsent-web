﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace eConsentWeb.Models
{
    public class CampaignDetailViewModel
    {
        public Campaign_Header Header { get; set; }
        public Mst_BU BusinessUnit { get; set; }
        public List<vCampaignBrand> Brands { get; set; }

        [Display(Name = "Target Audiences")]
        public List<vCampaignTA> TargetAudiences { get; set; }
        public List<DTWebinarChannel> Webinars { get; set; }
        public List<DTWebinarChannel> Emails { get; set; }
    }
}