﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace eConsentWeb.Models
{
    public class ConsentUserExportModels
    {
        public string DOCTOR_CODE { get; set; }
        public string NAME { get; set; }
        public string SPECIALITY { get; set; }
        public string CONSENT_LEVEL { get; set; }

        public string STR { get; set; }
        public string GENDER { get; set; }
        public string KOL_STATUS { get; set; }
        public string PROVINCE { get; set; }
        public string CITY { get; set; }
        public string MAIN_PRACTICE { get; set; }
        public string PRACTICE1 { get; set; }
        public string PRACTICE2 { get; set; }
        public string MLD_STATUS { get; set; }
        public string CONSENT_STATUS { get; set; }
    }

    public class ConsentAdminExportModels : ConsentUserExportModels
    {
        public string SERIES_ID { get; set; }
        public string CONSENT_CHANNEL { get; set; }
        public string CONSENT_DETAIL { get; set; }
        public string OPT_STATUS { get; set; }

        [DisplayFormat(DataFormatString = "{0:dd/MM/yyyy HH:mm:ss}")]
        public Nullable<System.DateTime> CONSENT_DATE { get; set; }
    }

    public class ConsentIndexViewModel
    {
        public string ID { get; set; }
        public string TITLE_ID { get; set; }
        public string DOCTOR_CODE { get; set; }
        public string NAME { get; set; }
        public string SPECIALITY { get; set; }
        public string SPECIALITY1 { get; set; }
        public string SPECIALITY2 { get; set; }
        public string STR { get; set; }
        public string GENDER { get; set; }
        public string KOL_STATUS { get; set; }
        public string PROVINCE { get; set; }
        public string CITY { get; set; }
        public string MAIN_PRACTICE { get; set; }
        public string PRACTICE1 { get; set; }
        public string PRACTICE2 { get; set; }
        public string MLD_STATUS { get; set; }
        public string CONSENT_STATUS { get; set; }
        public string OPT_STATUS { get; set; }

        public string CONSENT_CHANNEL { get; set; }
        public string[] CONSENT_CHANNEL_ARR { get; set; }
    }

    public class ConsentDetailsViewModel
    {
        public MS_eConsent MSConsent { get; set; }
        public MS_eConsent_HCPProfile HCPProfile { get; set; }
        public List<TR_eConsent> TRConsentList { get; set; }
    }

    public class TR_eConsentPivot
    {
        public string doctorCode { get; set; }
        public int phoneNumberCount { get; set; }
        public int emailCount { get; set; }
    }
}