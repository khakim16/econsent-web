﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace eConsentWeb
{
    public class MS_UserMetadata
    {
        public int id { get; set; }

        [Required(ErrorMessage = "Username is required")]
        [Display(Name = "Username")]
        public string username { get; set; }

        [Required(ErrorMessage = "Fullname is required")]
        [Display(Name = "Fullname")]
        public string fullname { get; set; }

        [Required(ErrorMessage = "Role is required")]
        [Display(Name = "Role")]
        public int role { get; set; }
        public string password { get; set; }

        [RegularExpression("^[a-zA-Z0-9_\\+-]+(\\.[a-zA-Z0-9_\\+-]+)*@[a-zA-Z0-9-]+(\\.[a-zA-Z0-9]+)*\\.([a-zA-Z]{2,4})$", ErrorMessage = "Invalid email format")]
        [Display(Name = "Email")]
        public string email { get; set; }

        [Display(Name = "Status")]
        public int status { get; set; }

        [Display(Name = "Created Date")]
        [DisplayFormat(DataFormatString = "{0:dd/MM/yyyy HH:mm:ss}")]
        public Nullable<System.DateTime> createdDate { get; set; }
        public int createdBy { get; set; }
    }

    public class MS_eConsentMetadata
    {
        public int id { get; set; }

        [Display(Name = "Doctor Code")]
        public string doctorCode { get; set; }

        [Display(Name = "Name")]
        public string doctor { get; set; }

        [Display(Name = "Speciality")]
        public string specialty { get; set; }

        [Display(Name = "Consent Level")]
        public string consentLevel { get; set; }

        [Display(Name = "Email Me Copy?")]
        public string emailMecopy { get; set; }

        [Display(Name = "Email Me Copy Address")]
        public string emailMecopyAddress { get; set; }

        [Display(Name = "Email Me Copy Date")]
        public Nullable<System.DateTime> emailMecopyDate { get; set; }

        [Display(Name = "Email Me Copy Open?")]
        public string emailMecopyOpen { get; set; }

        [Display(Name = "Team")]
        public string team { get; set; }

        [Display(Name = "Alignment Code")]
        public string alignmentCode { get; set; }

        [Display(Name = "Territory Name")]
        public string territoryName { get; set; }

        [Display(Name = "Employee Name")]
        public string employeeName { get; set; }

        [Display(Name = "Created Date")]
        [DisplayFormat(DataFormatString = "{0:dd/MM/yyyy HH:mm:ss}")]
        public Nullable<System.DateTime> createDate { get; set; }

        [Display(Name = "Created By")]
        public string usercreate { get; set; }
    }

    public class MS_eConsent_HCPProfileMetadata
    {

        public int id { get; set; }

        [Display(Name = "Title ID")]
        public string titleID { get; set; }
        
        [Display(Name = "Doctor Code")]
        public string doctorCode { get; set; }

        [Display(Name = "Name")]
        public string name { get; set; }

        [Display(Name = "Speciality 1")]
        public string speciality1 { get; set; }
        
        [Display(Name = "Speciality 2")]
        public string speciality2 { get; set; }

        [Display(Name = "STR")]
        public string str { get; set; }

        [Display(Name = "Gender")]
        public string gender { get; set; }

        [Display(Name = "KOL Status")]
        public string KOLstatus { get; set; }

        [Display(Name = "Province")]
        public string province { get; set; }

        [Display(Name = "City")]
        public string city { get; set; }

        [Display(Name = "Main Practice")]
        public string mainPractice { get; set; }

        [Display(Name = "Practice 1")]
        public string practice1 { get; set; }

        [Display(Name = "Sanofi ID")]
        public string practice2 { get; set; }

        [Display(Name = "MLD Status")]
        public string MLDstatus { get; set; }

        [Display(Name = "Consent Status")]

        public string consentStatus { get; set; }

        [Display(Name = "Created Date")]
        [DisplayFormat(DataFormatString = "{0:dd/MM/yyyy HH:mm:ss}")]
        public Nullable<System.DateTime> createDate { get; set; }
        
        [Display(Name = "Created By")]
        public string userCreate { get; set; }
    }

    public class TR_eConsentMetadata
    {
        public int id { get; set; }
        
        [Display(Name = "Series ID")]
        public string seriesID { get; set; }
        
        [Display(Name = "Consent ID")]
        public string consentID { get; set; }

        [Display(Name = "Doctor Code")]
        public string doctorCode { get; set; }

        [Display(Name = "Consent Level")]
        public string consentLevel { get; set; }

        [Display(Name = "Consent Channels")]
        public string consentChannels { get; set; }

        [Display(Name = "Consent Details")]
        public string consentDetails { get; set; }

        [Display(Name = "Email Me Copy Address")]
        public string emailMecopyAddress { get; set; }

        [Display(Name = "Opt Status")]
        public string optStatus { get; set; }

        [Display(Name = "Consent Date")]
        [DisplayFormat(DataFormatString = "{0:dd/MM/yyyy HH:mm:ss}")]
        public Nullable<System.DateTime> consentDate { get; set; }

        [Display(Name = "PDF Filename")]
        public string pdfFileName { get; set; }

        [Display(Name = "Created Date")]
        [DisplayFormat(DataFormatString = "{0:dd/MM/yyyy HH:mm:ss}")]
        public Nullable<System.DateTime> createDate { get; set; }
        
        [Display(Name = "Created By")]
        public string usercreate { get; set; }

        [Display(Name = "Flag Active")]
        public Nullable<bool> flagActive { get; set; }
    }

    /*
    public class CUSTOMERMetadata
    {
        public int CUSTOMERID { get; set; }

        [Required(ErrorMessage = "Customer Code is required")]
        [Display(Name = "Customer Code")]
        public string CUSTOMERCODE { get; set; }

        [Required(ErrorMessage = "Fullname is required")]
        [Display(Name = "Fullname")]
        public string FULLNAME { get; set; }

        [Required(ErrorMessage = "Gender is required")]
        [Display(Name = "Gender")]
        public Nullable<int> GENDER { get; set; }
        
        [Required(ErrorMessage = "Phone Number is required")]
        [RegularExpression("^[0-9]*$", ErrorMessage = "Phone Number must be a numeric")]
        [Display(Name = "Phone Number")]
        public string PHONENUMBER { get; set; }

        [Required(ErrorMessage = "City is required")]
        [Display(Name = "City")]
        public string CITY { get; set; }

        [Required(ErrorMessage = "Address is required")]
        [DataType(DataType.MultilineText)]
        [Display(Name = "Address")]
        public string ADDRESS { get; set; }

        [Required(ErrorMessage = "Email is required")]
        [RegularExpression("^[a-zA-Z0-9_\\+-]+(\\.[a-zA-Z0-9_\\+-]+)*@[a-zA-Z0-9-]+(\\.[a-zA-Z0-9]+)*\\.([a-zA-Z]{2,4})$", ErrorMessage = "Invalid email format")]
        [Display(Name = "Email")]
        public string EMAIL { get; set; }

        [Required(ErrorMessage = "Place Of Practice 1 is required")]
        [DataType(DataType.MultilineText)]
        [Display(Name = "Place Of Practice 1")]
        public string PLACEOFPRACTICE1 { get; set; }
        
        [Display(Name = "Place Of Practice 2")]
        [DataType(DataType.MultilineText)]
        public string PLACEOFPRACTICE2 { get; set; }
        
        [Display(Name = "Place Of Practice 3")]
        [DataType(DataType.MultilineText)]
        public string PLACEOFPRACTICE3 { get; set; }

        [Required(ErrorMessage = "Status is required")]
        [Display(Name = "Status")]
        public Nullable<int> STATUS { get; set; }
        
        [Display(Name = "Created Date")]
        [DisplayFormat(DataFormatString = "{0:dd/MM/yyyy HH:mm:ss}")]
        public System.DateTime CREATEDDATE { get; set; }
        public int CREATEDBY { get; set; }
    }

    public class PHARMACYMetadata
    {
        public int PHARMACYID { get; set; }

        [Required(ErrorMessage = "Channel Code is required")]
        [Display(Name = "Channel Code")]
        public string PHARMACYCODE { get; set; }

        [Required(ErrorMessage = "Channel Name is required")]
        [Display(Name = "Channel Name")]
        public string PHARMACYNAME { get; set; }

        [Required(ErrorMessage = "Channel Type is required")]
        [Display(Name = "Channel Type")]
        public string TYPE { get; set; }

        [Required(ErrorMessage = "Phone Number is required")]
        [RegularExpression("^[0-9]*$", ErrorMessage = "Phone Number must be a numeric")]
        [Display(Name = "Phone Number")]
        public string PHONENUMBER { get; set; }

        [Required(ErrorMessage = "Email is required")]
        [RegularExpression("^[a-zA-Z0-9_\\+-]+(\\.[a-zA-Z0-9_\\+-]+)*@[a-zA-Z0-9-]+(\\.[a-zA-Z0-9]+)*\\.([a-zA-Z]{2,4})$", ErrorMessage = "Invalid email format")]
        [Display(Name = "Email")]
        public string EMAIL { get; set; }

        [Required(ErrorMessage = "City is required")]
        [Display(Name = "City")]
        public string CITY { get; set; }

        [Required(ErrorMessage = "Address is required")]
        [DataType(DataType.MultilineText)]
        [Display(Name = "Address")]
        public string ADDRESS { get; set; }

        //[Required(ErrorMessage = "Plant is required")]
        [Display(Name = "Plant")]
        public string PHARMACYPLANT { get; set; }

        [Required(ErrorMessage = "Status is required")]
        [Display(Name = "Status")]
        public Nullable<int> STATUS { get; set; }

        [Display(Name = "Created Date")]
        [DisplayFormat(DataFormatString = "{0:dd/MM/yyyy HH:mm:ss}")]
        public System.DateTime CREATEDDATE { get; set; }
        public int CREATEDBY { get; set; }
    }

    public class ORDERMetadata
    {
        [Display(Name = "Order ID")]
        public int ORDERID { get; set; }

        [Required(ErrorMessage = "Customer is required")]
        public int CUSTOMERID { get; set; }

        [Required(ErrorMessage = "Channel is required")]
        public int PHARMACYID { get; set; }

        [Display(Name = "Delivery Date")]
        [DisplayFormat(DataFormatString = "{0:MM/dd/yyyy}")]
        public Nullable<System.DateTime> DELIVERYDATE { get; set; }

        [Display(Name = "Delivery Address")]
        public string DELIVERYADDRESS { get; set; }

        [Display(Name = "Order City")]
        public string CITY { get; set; }

        [Display(Name = "Distributor Name")]
        public string DISTRIBUTORNAME { get; set; }
        
        [RegularExpression("^[a-zA-Z0-9_\\+-]+(\\.[a-zA-Z0-9_\\+-]+)*@[a-zA-Z0-9-]+(\\.[a-zA-Z0-9]+)*\\.([a-zA-Z]{2,4})$", ErrorMessage = "Invalid email format")]
        [Display(Name = "Distributor Email")]
        public string DISTRIBUTOREMAIL { get; set; }

        [RegularExpression("^[0-9]*$", ErrorMessage = "Phone Number must be a numeric")]
        [Display(Name = "Distributor Phone")]
        public string DISTRIBUTORPHONENUMBER { get; set; }

        [Display(Name = "Created Date")]
        [DisplayFormat(DataFormatString = "{0:dd/MM/yyyy HH:mm:ss}")]
        public System.DateTime CREATEDDATE { get; set; }
        public int CREATEDBY { get; set; }

        [Display(Name = "Status")]
        public int STATUS { get; set; }

        [Display(Name = "Reject Reason")]
        public int REJECTREASON { get; set; }
    }
    */
} 