﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace eConsentWeb.Models
{
    public class CampaignModel
    {

        [Display(Name = "Campaign ID")]
        public string CampaignID { get; set; }

        [Required]
        [Display(Name = "Campaign Name")]
        public string CampaignName { get; set; }

        [Required]
        [Display(Name = "Business Unit")]
        public string BusinessUnitID { get; set; }

        [Required]
        [Display(Name = "Start Date")]
        public DateTime? StartDate { get; set; }

        [Required]
        [Display(Name = "End Date")]
        public DateTime? EndDate { get; set; }

        [Required]
        [Display(Name = "Brand")]
        public int[] BrandID { get; set; }

        [Required]
        [Display(Name = "Target Audience")]
        public int[] TargetAudienceID { get; set; }

        [Display(Name = "Status")]
        public string Status { get; set; }

        [Display(Name = "Description")]
        public string Description { get; set; }

        [Display(Name = "Channel")]
        public string Channel { get; set; }

        public List<DTWebinarChannel> Webinars;

        public List<DTWebinarChannel> Emails;

    }

    public class DTWebinarChannel
    {
        public bool isChecked { get; set; }
        public string WebinarID { get; set; }
        public string Topic { get; set; }
        [DisplayFormat(ApplyFormatInEditMode = true, DataFormatString = "{0:dd/MM/yyyy}")]
        public DateTime? EventDate { get; set; }
        public string Source { get; set; }
        public int? ChanleID { get; set; }
    }

    public class WebinarCampaignView
    {
        public string CampaignID { get; set; }
        public string MeetingID { get; set; }
        public string BU { get; set; }
        public string Channel { get; set; }
        public string Topic { get; set; }
        public int? ChanleID { get; set; }
        public DateTime? Event_Date { get; set; }

    }


}