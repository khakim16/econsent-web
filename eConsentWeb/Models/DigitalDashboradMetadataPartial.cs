﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace eConsentWeb.Models
{
    [MetadataType(typeof(CampaignHeaderMetadata))]
    public partial class Campaign_Header { }

    [MetadataType(typeof(BusinessUnitMetadata))]
    public partial class Mst_BU { }
}