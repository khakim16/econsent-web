﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace eConsentWeb.Models
{
    public class AdvancedSearchRequest
    {
        // id
        public string i { get; set; }

        // name
        public string n { get; set; }

        // business unit
        public string[] bu { get; set; }

        // brand
        public string[] b { get; set; }

        // period start
        public string ps { get; set; }

        // period end
        public string pe { get; set; }

        // status
        public string s { get; set; }

        // channel
        public string[] c { get; set; }

        // target audience
        public string[] ta { get; set; }
    }
}