﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace eConsentWeb.Models
{
    public class DataTablesViewModel
    {
        public int draw { get; set; }
        public string sortColumn { get; set; }
        public string sortColumnDir { get; set; }
        public int offset { get; set; }
        public int size { get; set; }
        public string keyword { get; set; }
    }
}