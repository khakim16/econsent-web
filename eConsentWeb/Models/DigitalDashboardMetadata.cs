﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace eConsentWeb.Models
{
    // Digital Dashboard
    public class CampaignHeaderMetadata
    {
        [Display(Name = "Campaign ID")]
        public string CampaignID { get; set; }

        [Display(Name = "Campaign Name")]
        public string CampaignName { get; set; }

        [Display(Name = "Description")]
        public string CampaignDescription { get; set; }
        public Nullable<int> BUID { get; set; }

        [Display(Name = "Start Date")]
        [DisplayFormat(DataFormatString = "{0:yyyy-MM-dd}")]
        public Nullable<System.DateTime> StartDate { get; set; }

        [Display(Name = "Finish Date")]
        [DisplayFormat(DataFormatString = "{0:yyyy-MM-dd}")]
        public Nullable<System.DateTime> EndDate { get; set; }
        public string Status { get; set; }
        public Nullable<System.DateTime> User_Updated { get; set; }
        public Nullable<System.DateTime> Date_Updated { get; set; }
    }

    public class BusinessUnitMetadata
    {
        public int BUID { get; set; }

        [Display(Name = "Business Unit")]
        public string BU { get; set; }
        public string Status { get; set; }
        public string User_Updated { get; set; }
        public Nullable<System.DateTime> Date_Updated { get; set; }
    }
}