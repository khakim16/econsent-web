//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace eConsentWeb.Models
{
    using System;
    using System.Collections.Generic;
    
    public partial class vChannelWebinarHeader
    {
        public string WebinarID { get; set; }
        public string Topic { get; set; }
        public Nullable<System.DateTime> Start_Date { get; set; }
        public Nullable<System.DateTime> End_Date { get; set; }
        public Nullable<double> Duration { get; set; }
        public Nullable<double> Participant { get; set; }
        public string Source { get; set; }
        public string ChannelType { get; set; }
        public Nullable<int> ChannelID { get; set; }
    }
}
