﻿using eConsentWeb.Common;
using eConsentWeb.Models;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;

namespace eConsentWeb.Service
{
    public interface IChannelDownloader
    {
        DataSet GetDataSet(string id);
    }

    class SanofiZoomDownloader : IChannelDownloader
    {
        private DigitalDashboardEntities db = new DigitalDashboardEntities();

        public DataSet GetDataSet(string id)
        {
            var connString = db.Database.Connection.ConnectionString;
            SQLConnect conn = new SQLConnect(connString);
            
            var queryGet = "select * from {0} where WebinarID = '{1}'";

            var ds = new DataSet();

            // AttendeeReport
            var sqlGetAttendeeReport = string.Format(queryGet, "vZoomAttendeeReport", id);
            var attendeeReportDt = conn.Get_DataTable(CommandType.Text, sqlGetAttendeeReport, null);
            attendeeReportDt.TableName = "AttendeeReport";
            ds.Tables.Add(attendeeReportDt);

            // AttendeeDetail
            var sqlGetAttendeeDetail = string.Format(queryGet, "vZoomAttendeeDetail", id);
            var attendeeDetailDt = conn.Get_DataTable(CommandType.Text, sqlGetAttendeeDetail, null);
            attendeeDetailDt.TableName = "AttendeeDetail";
            ds.Tables.Add(attendeeDetailDt);

            // PanelistDetail
            var sqlGetPanelistDetail = string.Format(queryGet, "vZoomPanelistDetail", id);
            var PanelistDetail = conn.Get_DataTable(CommandType.Text, sqlGetPanelistDetail, null);
            PanelistDetail.TableName = "PanelistDetail";
            ds.Tables.Add(PanelistDetail);

            // OtherAttendee
            var sqlGetOtherAttendee = string.Format(queryGet, "vZoomOtherAttendee", id);
            var OtherAttendeeDt = conn.Get_DataTable(CommandType.Text, sqlGetOtherAttendee, null);
            OtherAttendeeDt.TableName = "OtherAttendee";
            ds.Tables.Add(OtherAttendeeDt);
            
            return ds;
        }
    }

    class RepublicMDownloader : IChannelDownloader
    {
        private DigitalDashboardEntities db = new DigitalDashboardEntities();

        public DataSet GetDataSet(string id)
        {
            var connString = db.Database.Connection.ConnectionString;
            SQLConnect conn = new SQLConnect(connString);

            var queryGet = "select * from {0} where MeetingID = '{1}'";

            var ds = new DataSet();

            // Webinar
            var sqlGetWebinar = string.Format(queryGet, "vRepmWebinar", id);
            var WebinarDt = conn.Get_DataTable(CommandType.Text, sqlGetWebinar, null);
            WebinarDt.TableName = "Webinar";
            ds.Tables.Add(WebinarDt);

            // Participant
            var sqlGetParticipant = string.Format(queryGet, "vRepmParticipant", id);
            var ParticipantDt = conn.Get_DataTable(CommandType.Text, sqlGetParticipant, null);
            ParticipantDt.TableName = "Participant";
            ds.Tables.Add(ParticipantDt);

            // Registered
            var sqlGetRegistered = string.Format(queryGet, "vRepmRegistered", id);
            var Registered = conn.Get_DataTable(CommandType.Text, sqlGetRegistered, null);
            Registered.TableName = "Registered";
            ds.Tables.Add(Registered);

            // EmailCampaign
            var sqlGetEmailCampaign = string.Format(queryGet, "vRepmEmailCampaign", id);
            var EmailCampaignDt = conn.Get_DataTable(CommandType.Text, sqlGetEmailCampaign, null);
            EmailCampaignDt.TableName = "EmailCampaign";
            ds.Tables.Add(EmailCampaignDt);

            return ds;
        }
    }

    class PigeonHoleDownloader : IChannelDownloader
    {
        private DigitalDashboardEntities db = new DigitalDashboardEntities();

        public DataSet GetDataSet(string id)
        {
            var connString = db.Database.Connection.ConnectionString;
            SQLConnect conn = new SQLConnect(connString);

            var queryGet = "select * from {0} where WebinarID = '{1}'";

            var ds = new DataSet();

            // FeedbackSurvey
            var sqlGetFeedbackSurvey = string.Format(queryGet, "vPHFeedbackSurvey", id);
            var FeedbackSurveyDt = conn.Get_DataTable(CommandType.Text, sqlGetFeedbackSurvey, null);
            FeedbackSurveyDt.TableName = "FeedbackSurvey";
            ds.Tables.Add(FeedbackSurveyDt);

            // QAStatistics
            var sqlGetQAStatistics = string.Format(queryGet, "vPHQAStatistics", id);
            var QAStatisticsDt = conn.Get_DataTable(CommandType.Text, sqlGetQAStatistics, null);
            QAStatisticsDt.TableName = "QAStatistics";
            ds.Tables.Add(QAStatisticsDt);

            // QAComments
            var sqlGetQAComments = string.Format(queryGet, "vPHQAComments", id);
            var QAComments = conn.Get_DataTable(CommandType.Text, sqlGetQAComments, null);
            QAComments.TableName = "QAComments";
            ds.Tables.Add(QAComments);

            // QAQuestions
            var sqlGetQAQuestions = string.Format(queryGet, "vPHQAQuestions", id);
            var QAQuestionsDt = conn.Get_DataTable(CommandType.Text, sqlGetQAQuestions, null);
            QAQuestionsDt.TableName = "QAQuestions";
            ds.Tables.Add(QAQuestionsDt);

            return ds;
        }
    }

    class MailChimpDownloader : IChannelDownloader
    {
        private DigitalDashboardEntities db = new DigitalDashboardEntities();

        public DataSet GetDataSet(string id)
        {
            var connString = db.Database.Connection.ConnectionString;
            SQLConnect conn = new SQLConnect(connString);

            var queryGet = "select * from {0} where MeetingID = '{1}'";

            var ds = new DataSet();

            // Detail
            var sqlGetDetail = string.Format(queryGet, "vMailChimpDetail", id);
            var DetailDt = conn.Get_DataTable(CommandType.Text, sqlGetDetail, null);
            DetailDt.TableName = "Detail";
            ds.Tables.Add(DetailDt);
            
            return ds;
        }
    }

    class AdobeCampaignDownloader : IChannelDownloader
    {
        private DigitalDashboardEntities db = new DigitalDashboardEntities();

        public DataSet GetDataSet(string id)
        {
            var connString = db.Database.Connection.ConnectionString;
            SQLConnect conn = new SQLConnect(connString);

            var queryGet = "select * from {0} where MeetingID = '{1}'";

            var ds = new DataSet();

            // AdobeCampaign
            var sqlGetAdobeCampaign = string.Format(queryGet, "vAdobe_Campaign", id);
            var AdobeCampaignDt = conn.Get_DataTable(CommandType.Text, sqlGetAdobeCampaign, null);
            AdobeCampaignDt.TableName = "AdobeCampaign";
            ds.Tables.Add(AdobeCampaignDt);

            return ds;
        }
    }
}