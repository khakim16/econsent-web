﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using eConsentWeb;
using eConsentWeb.Common;
using System.Data.Entity.Validation;
using Cryptography;

namespace eConsentWeb.Controllers
{
    public class UserManagementController : Controller
    {
        private eConsentEntities db = new eConsentEntities();
        private AES_Cryptography crypto = new AES_Cryptography();

        // GET: UserManagement
        public ActionResult Index()
        {
            var uSERs = db.MS_User.Include(u => u.MS_User2);
            return View(uSERs.ToList());
        }

        // GET: UserManagement/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            MS_User uSER = db.MS_User.Find(id);
            if (uSER == null)
            {
                return HttpNotFound();
            }
            return View(uSER);
        }

        // GET: UserManagement/Create
        public ActionResult Create()
        {
            GenerateDDL();

            return View();
        }

        // POST: UserManagement/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create(MS_User uSER)
        {
            MS_User chkUser = db.MS_User.Where(w => w.username == uSER.username).FirstOrDefault();
            GenerateDDL();

            if (chkUser != null)
            {
                ModelState.AddModelError("username", "Username is already exists");
            }

            if (ModelState.IsValid)
            {
                uSER.password = crypto.encryptString(UserHelper.DefaultPassword);
                uSER.createdBy = Convert.ToInt16(Session["UserID"].ToString());
                uSER.createdDate = DateTime.Now;

                try
                {
                    db.MS_User.Add(uSER);
                    db.SaveChanges();
                }
                catch (Exception ex)
                {
                    ViewBag.Error = "Create New User Failed";
                    LogSystem.ErrorLog(ex, null);
                    //throw;
                    return View(uSER);
                }

                return RedirectToAction("Index");
            }
            
            return View(uSER);
        }

        // GET: UserManagement/Edit/5
        public ActionResult Edit(int? id)
        {
            GenerateDDL();

            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            MS_User uSER = db.MS_User.Find(id);
            if (uSER == null)
            {
                return HttpNotFound();
            }

            return View(uSER);
        }

        // POST: UserManagement/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit(MS_User uSER, bool IsReset)
        {
            GenerateDDL();

            if (ModelState.IsValid)
            {
                if (IsReset)
                    uSER.password = crypto.encryptString(UserHelper.DefaultPassword);
                
                try
                {
                    db.Entry(uSER).State = EntityState.Modified;
                    db.SaveChanges();
                }
                catch (Exception ex)
                {
                    ViewBag.Error = "Update User Failed";
                    LogSystem.ErrorLog(ex, null);
                    //throw;
                    return View(uSER);
                }

                return RedirectToAction("Index");
            }
            
            return View(uSER);
        }

        // GET: UserManagement/Delete/5
        //public ActionResult Delete(int? id)
        //{
        //    if (id == null)
        //    {
        //        return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
        //    }
        //    MS_User uSER = db.MS_User.Find(id);
        //    if (uSER == null)
        //    {
        //        return HttpNotFound();
        //    }
        //    return View(uSER);
        //}

        // POST: UserManagement/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            MS_User uSER = db.MS_User.Find(id);
            db.MS_User.Remove(uSER);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        private void GenerateDDL()
        {
            ViewBag.Roles = EnumHelper.ToOrderedSelectList<UserRole>();
            ViewBag.Statuses = EnumHelper.ToOrderedSelectList<Status>();
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
