﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using eConsentWeb.Common;
using eConsentWeb.Models;
using System.Data;
using Newtonsoft.Json;
using System.Net;
using System.Data.Entity;
using System.Web.UI;
using eConsentWeb.Service;

namespace eConsentWeb.Controllers
{
    public class CampaignController : ParentController
    {
        private DigitalDashboardEntities db = new DigitalDashboardEntities();

        private string vCampaignHeader = "vCampaignHeader";
        private string vCampaignWebinarDetail = "vCampaignWebinarDetail";
        private string vCampaignEmailDetail = "vCampaignEmailDetail";

        #region Generate DDL
        private MultiSelectList GenerateDDLBusinessUnit(string[] selectedValue)
        {
            var data = db.Mst_BU.Where(w => w.Status.Equals(((int)Status.Active).ToString())).ToList();

            List<SelectListItem> items = data
                        .Select(n =>
                        new SelectListItem
                        {
                            Value = n.BUID.ToString(),
                            Text = n.BU
                        }).ToList();

            return new MultiSelectList(items, "Value", "Text", selectedValue);
        }

        private MultiSelectList GenerateDDLBrand(string[] selectedValue)
        {
            var data = db.Mst_Brand.Where(w => w.Status.Equals(((int)Status.Active).ToString())).ToList();

            List<SelectListItem> items = data
                        .Select(n =>
                        new SelectListItem
                        {
                            Value = n.BrandID.ToString(),
                            Text = n.BrandName
                        }).ToList();

            return new MultiSelectList(items, "Value", "Text", selectedValue);
        }

        private SelectList GenerateDDLCampaignStatus(string selectedValue)
        {
            var data = EnumHelper.ToOrderedSelectList<CampaignStatus>();

            return new SelectList(data, "Value", "Text", selectedValue);
        }

        private MultiSelectList GenerateDDLChannel(string[] selectedValue)
        {
            var data = db.Mst_Channel.Where(w => w.Status.Equals(((int)Status.Active).ToString())).ToList();

            List<SelectListItem> items = data
                        .Select(n =>
                        new SelectListItem
                        {
                            Value = n.ChannelID.ToString(),
                            Text = n.Channel
                        }).ToList();

            return new MultiSelectList(items, "Value", "Text", selectedValue);
        }

        private MultiSelectList GenerateDDLTargetAudience(string[] selectedValue)
        {
            var data = db.Mst_Target_Audience.Where(w => w.Status.Equals(((int)Status.Active).ToString())).ToList();

            List<SelectListItem> items = data
                        .Select(n =>
                        new SelectListItem
                        {
                            Value = n.TargetAudienceID.ToString(),
                            Text = n.TargetAudience
                        }).ToList();

            return new MultiSelectList(items, "Value", "Text", selectedValue);
        }

        private void GenerateDDL(string campaignID)
        {
            ViewBag.Roles = EnumHelper.ToOrderedSelectList<UserRole>();
            ViewBag.Statuses = EnumHelper.ToOrderedSelectList<Status>();

            //ViewBag.BU = db.Mst_BU.Select(s => new SelectListItem
            //{
            //    Text = s.BU,
            //    Value = s.BUID.ToString()
            //});

            var BusinessUnit = db.Campaign_Header.Where(w => w.CampaignID == campaignID).Select(s => s.BUID.ToString()).ToArray();
            ViewBag.BusinessUnit = GenerateDDLBusinessUnit(BusinessUnit);

            var BrandList = db.Campaign_Brand.Where(w => w.CampaignID == campaignID).Select(s => s.BrandID.ToString()).ToArray();
            ViewBag.Brand = GenerateDDLBrand(BrandList);

            var TargetAudience = db.Campaign_TargetAudience.Where(w => w.CampaignID == campaignID).Select(s => s.TargetAudienceID.ToString()).ToArray();
            ViewBag.TargetAudience = GenerateDDLTargetAudience(TargetAudience);


        }
        #endregion

        public ActionResult Index(AdvancedSearchRequest request)
        {
            ViewBag.BusinessUnit = GenerateDDLBusinessUnit(request.bu);
            ViewBag.Brand = GenerateDDLBrand(request.b);
            ViewBag.CampaignStatus = GenerateDDLCampaignStatus(request.s);
            ViewBag.Channel = GenerateDDLChannel(request.c);
            ViewBag.TargetAudience = GenerateDDLTargetAudience(request.ta);

            return View(request);
        }

        public ActionResult Create()
        {
            //if (ID ==0)
            //{
            //    return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            //}

            CampaignModel cm = new CampaignModel();

            //var campaignHeader = db.Campaign_Header.Where(w => w.ID == ID).FirstOrDefault();

            //if (campaignHeader == null)
            //{
            //    return HttpNotFound();
            //}
            //cm.ID = campaignHeader.ID;
            //cm.CampaignID = campaignHeader.CampaignID;
            //cm.CampaignName = campaignHeader.CampaignName;
            //cm.StartDate = campaignHeader.StartDate;
            //cm.EndDate = campaignHeader.EndDate;
            //cm.Description = campaignHeader.CampaignDescription;

            GenerateDDL("");

            return View(cm);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create(CampaignModel cm)
        {
            if (ModelState.IsValid)
            {
                using (var dbTran = db.Database.BeginTransaction())
                {
                    try
                    {
                        Campaign_Header campaign_Header = new Campaign_Header();
                        //campaign_Header = db.Campaign_Header.Where(w => w.ID == cm.ID).FirstOrDefault();
                        campaign_Header.CampaignID = GenerateCampaignID();
                        campaign_Header.CampaignName = cm.CampaignName;
                        campaign_Header.BUID = Int32.Parse(cm.BusinessUnitID);
                        campaign_Header.CampaignDescription = cm.Description;
                        campaign_Header.StartDate = cm.StartDate;
                        campaign_Header.EndDate = cm.EndDate;
                        campaign_Header.Status = CheckCampaignDetail(cm.CampaignID) == true ? "2" : "1";
                        campaign_Header.Date_Updated = DateTime.Now;
                        campaign_Header.User_Updated = Session["UserName"].ToString();

                        db.Campaign_Header.Add(campaign_Header);
                        db.SaveChanges();

                        foreach (var brand in cm.BrandID)
                        {
                            if (CheckBrandIsExist(campaign_Header.CampaignID, brand))
                            {
                                continue;
                            }
                            Campaign_Brand cb = new Campaign_Brand();
                            cb.CampaignID = campaign_Header.CampaignID;
                            cb.BrandID = brand;
                            cb.Status = ((int)Status.Active).ToString();
                            cb.Date_Updated = DateTime.Now;
                            cb.User_Updated = Session["UserName"].ToString();

                            db.Campaign_Brand.Add(cb);
                            db.SaveChanges();
                        }

                        foreach (var taID in cm.TargetAudienceID)
                        {
                            if (CheckTargetAudIsExist(campaign_Header.CampaignID, taID))
                            {
                                continue;
                            }

                            Campaign_TargetAudience ta = new Campaign_TargetAudience();
                            ta.CampaignID = campaign_Header.CampaignID;
                            ta.TargetAudienceID = taID;
                            ta.Status = ((int)Status.Active).ToString();
                            ta.Date_Updated = DateTime.Now;
                            ta.User_Updated = Session["UserName"].ToString();

                            db.Campaign_TargetAudience.Add(ta);
                            db.SaveChanges();
                        }
                        dbTran.Commit();
                        return RedirectToAction("Assign", new { id = campaign_Header.CampaignID });
                    }
                    catch (Exception ex)
                    {
                        ViewBag.Error = "Assign Failed";
                        LogSystem.ErrorLog(ex, null);
                        dbTran.Rollback();
                        GenerateDDL(cm.CampaignID);
                        return View(cm);
                    }
                }


            }
            GenerateDDL(cm.CampaignID);

            return View(cm);
        }

        public ActionResult AdvancedSearch(AdvancedSearchRequest request)
        {
            ViewBag.BusinessUnit = GenerateDDLBusinessUnit(request.bu);
            ViewBag.Brand = GenerateDDLBrand(request.b);
            ViewBag.CampaignStatus = GenerateDDLCampaignStatus(request.s);
            ViewBag.Channel = GenerateDDLChannel(request.c);
            ViewBag.TargetAudience = GenerateDDLTargetAudience(request.ta);

            return View(request);
        }

        public ActionResult Details(string id)
        {
            Campaign_Header header = db.Campaign_Header.Where(w => w.CampaignID.Equals(id) && !w.Status.Equals(((int)CampaignStatus.Deleted).ToString())).FirstOrDefault();

            if (header == null)
            {
                return HttpNotFound();
            }

            Mst_BU businessUnit = db.Mst_BU.Where(w => w.BUID == header.BUID).FirstOrDefault();
            List<vCampaignBrand> brands = db.vCampaignBrands.Where(w => w.CampaignID.Equals(id)).ToList();
            List<vCampaignTA> targetAudiences = db.vCampaignTAs.Where(w => w.CampaignID.Equals(id)).ToList();

            List<DTWebinarChannel> webinars = db.vCampaignWebinarDetails.Where(w => w.CampaignID.Equals(id)).Select(s => new DTWebinarChannel
            {
                WebinarID = s.MeetingID,
                Topic = s.Topic,
                EventDate = s.Event_Date,
                Source = s.Source,
            }).AsEnumerable().ToList();


            List<DTWebinarChannel> emails = db.vCampaignEmailDetails.Where(w => w.CampaignID.Equals(id)).Select(s => new DTWebinarChannel
            {
                WebinarID = s.MeetingID,
                Topic = s.Topic,
                EventDate = s.Event_Date,
                Source = s.Source,
            }).AsEnumerable().ToList();

            CampaignDetailViewModel cdvm = new CampaignDetailViewModel();
            cdvm.Header = header;
            cdvm.BusinessUnit = businessUnit;
            cdvm.Brands = brands;
            cdvm.Emails = emails;
            cdvm.TargetAudiences = targetAudiences;
            cdvm.Webinars = webinars;

            return View(cdvm);
        }

        [HttpPost]
        public ActionResult LoadSimpleSearch()
        {
            var connString = db.Database.Connection.ConnectionString;
            SQLConnect conn = new SQLConnect(connString);

            JsonResult result = new JsonResult();

            // datatables parameters
            DataTablesViewModel parameter = GetParameter();

            var queryGet = "select * from {0} where [Status] <> '0' and [Status] <> '-1' {1} order by {2} offset {3} rows fetch next {4} rows only";
            var queryCount = "select count(*) from {0} where [Status] <> '0' {1}";

            var order = "1";

            string sqlCount = string.Format(queryCount, vCampaignHeader, "");
            string recordsTotal = conn.ExecuteScalar(CommandType.Text, sqlCount, null);


            // generate where query
            string where = getFilterQueryCampaignHeader(parameter.keyword, parameter.keyword, new[] { parameter.keyword }, new[] { parameter.keyword }, parameter.keyword, parameter.keyword, parameter.keyword, new[] { parameter.keyword }, new[] { parameter.keyword }, false);


            // order data
            if (!String.IsNullOrEmpty(parameter.sortColumn))
            {
                order = string.Format("[{0}] {1}", parameter.sortColumn, parameter.sortColumnDir);
            }

            string sqlGet = string.Format(queryGet, vCampaignHeader, where, order, parameter.offset, parameter.size);

            DataTable dt = conn.Get_DataTable(CommandType.Text, sqlGet, null);

            sqlCount = string.Format(queryCount, vCampaignHeader, where);
            string recordsFiltered = conn.ExecuteScalar(CommandType.Text, sqlCount, null);

            // result
            string JSONString = JsonConvert.SerializeObject(dt);
            var values = JsonConvert.DeserializeObject<List<Dictionary<string, string>>>(JSONString);

            result = Json(new { recordsFiltered = recordsFiltered, recordsTotal = recordsTotal, data = values, draw = parameter.draw }, JsonRequestBehavior.AllowGet);

            return result;
        }

        [HttpPost]
        public ActionResult LoadAdvancedSearch()
        {
            var connString = db.Database.Connection.ConnectionString;
            SQLConnect conn = new SQLConnect(connString);

            JsonResult result = new JsonResult();

            // datatables parameters
            DataTablesViewModel parameter = GetParameter();

            // custom search
            var campaignId = Request.Form.GetValues("i")?.FirstOrDefault();
            var name = Request.Form.GetValues("n")?.FirstOrDefault();
            var businessUnit = Request.Form.GetValues("bu[]");
            var brand = Request.Form.GetValues("b[]");
            var periodStart = Request.Form.GetValues("ps")?.FirstOrDefault();
            var periodEnd = Request.Form.GetValues("pe")?.FirstOrDefault();
            var status = Request.Form.GetValues("s")?.FirstOrDefault();
            var channel = Request.Form.GetValues("c[]");
            var targetAudience = Request.Form.GetValues("ta[]");

            var queryGet = "select * from {0} where [Status] <> '0' and [Status] <> '-1' {1} order by {2} offset {3} rows fetch next {4} rows only";
            var queryCount = "select count(*) from {0} where [Status] <> '0' and [Status] <> '-1' {1}";

            var order = "1";

            string sqlCount = string.Format(queryCount, vCampaignHeader, "");
            string recordsTotal = conn.ExecuteScalar(CommandType.Text, sqlCount, null);


            // generate where query (Advanced Search)
            string where = getFilterQueryCampaignHeader(campaignId, name, businessUnit, brand, periodStart, periodEnd, status, channel, targetAudience);

            if (String.IsNullOrEmpty(where))
            {
                // generate where query (Simple Search)
                where = getFilterQueryCampaignHeader(parameter.keyword, parameter.keyword, new[] { parameter.keyword }, new[] { parameter.keyword }, parameter.keyword, parameter.keyword, parameter.keyword, new[] { parameter.keyword }, new[] { parameter.keyword }, false);
            }

            // order data
            if (!String.IsNullOrEmpty(parameter.sortColumn))
            {
                order = string.Format("[{0}] {1}", parameter.sortColumn, parameter.sortColumnDir);
            }

            string sqlGet = string.Format(queryGet, vCampaignHeader, where, order, parameter.offset, parameter.size);

            DataTable dt = conn.Get_DataTable(CommandType.Text, sqlGet, null);

            sqlCount = string.Format(queryCount, vCampaignHeader, where);
            string recordsFiltered = conn.ExecuteScalar(CommandType.Text, sqlCount, null);

            // result
            string JSONString = JsonConvert.SerializeObject(dt);
            var values = JsonConvert.DeserializeObject<List<Dictionary<string, string>>>(JSONString);

            result = Json(new { recordsFiltered = recordsFiltered, recordsTotal = recordsTotal, data = values, draw = parameter.draw }, JsonRequestBehavior.AllowGet);

            return result;

        }

        private string getFilterQueryCampaignHeader(string campaignId, string name, string[] businessUnit, string[] brand, string periodStart, string periodEnd, string status, string[] channel, string[] targetAudience, bool isAnd = true)
        {
            var whereClause = new List<string>();

            #region Filter Data

            // Campaign ID
            if (!String.IsNullOrEmpty(campaignId))
            {
                whereClause.Add("[Campaign ID] like '%" + campaignId + "%'");
            }

            // Campaign Name
            if (!String.IsNullOrEmpty(name))
            {
                whereClause.Add("[Campaign Name] like '%" + name + "%'");
            }

            // Business Unit
            if (businessUnit?.Length > 0)
            {
                var buArr = new List<string>();
                foreach (var bu in businessUnit)
                {
                    buArr.Add("[Business Unit] like '%" + bu + "%'");
                }

                var buWhere = string.Join(" or ", buArr);
                whereClause.Add(string.Format("({0})", buWhere));
            }

            // Brand
            if (brand?.Length > 0)
            {
                var brandArr = new List<string>();
                foreach (var b in brand)
                {
                    brandArr.Add("[Brand] like '%" + b + "%'");
                }

                var brandWhere = string.Join(" or ", brandArr);
                whereClause.Add(string.Format("({0})", brandWhere));
            }

            // Period
            if (!String.IsNullOrEmpty(periodStart) || !String.IsNullOrEmpty(periodEnd))
            {
                whereClause.Add("([Start Date] <= '" + periodEnd + "' and [End Date] >= '" + periodStart + "')");
            }

            // Status
            if (!String.IsNullOrEmpty(status))
            {
                whereClause.Add("[Status] = '" + status + "'");
            }

            // Channel
            if (channel?.Length > 0)
            {
                var channelArr = new List<string>();
                foreach (var c in channel)
                {
                    channelArr.Add("[Channel] like '%" + c + "%'");
                }

                var channelWhere = string.Join(" or ", channelArr);
                whereClause.Add(string.Format("({0})", channelWhere));
            }

            // Target Audience
            if (targetAudience?.Length > 0)
            {
                var taArr = new List<string>();
                foreach (var c in targetAudience)
                {
                    taArr.Add("[Target Audience] like '%" + c + "%'");
                }

                var channelWhere = string.Join(" or ", taArr);
                whereClause.Add(string.Format("({0})", channelWhere));
            }

            #endregion

            var op = isAnd ? " and " : " or ";
            return whereClause.Count() > 0 ? " and (" + string.Join(op, whereClause) + ")" : "";
        }

        public void ExportAdvancedSearch(AdvancedSearchRequest req, string time)
        {
            var connString = db.Database.Connection.ConnectionString;
            SQLConnect conn = new SQLConnect(connString);

            string filename = "CampaignReport_" + DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss");

            string where = getFilterQueryCampaignHeader(req.i, req.n, req.bu, req.b, req.ps, req.pe, req.s, req.c, req.ta);
            var order = "1";

            var queryGet = "select * from {0} where [Status] <> '0' and [Status] <> '-1' {1} order by {2}";
            string sqlGet = string.Format(queryGet, vCampaignHeader, where, order);

            DataTable dt = conn.Get_DataTable(CommandType.Text, sqlGet, null);

            foreach (DataRow dr in dt.Rows)
            {
                var status = dr["Status"];
                var cStatus = Helpers.GetCampaignStatus(dr["Status"].ToString());
                dr.SetField("Status", cStatus);
            }

            // add a cookie with the name 'dlc' and the value from the postback
            Response.Cookies.Add(new HttpCookie("dlc", time));

            ExportToFile(dt, filename);
        }

        public void ExportExcelStatistic(string id, string time)
        {
            var connString = db.Database.Connection.ConnectionString;
            SQLConnect conn = new SQLConnect(connString);

            Campaign_Header header = db.Campaign_Header.Where(w => w.CampaignID.Equals(id)).FirstOrDefault();

            var filename = header.CampaignID + "-" + header.CampaignName + "-" + DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss");

            // Summary
            var sqlGetSummary = string.Format("select * from {0} where [Campaign ID] = '{1}'", "vCampaignSummary", id);
            var summaryDt = conn.Get_DataTable(CommandType.Text, sqlGetSummary, null);
            summaryDt.TableName = "Campaign_Summary";

            // Webinar
            var sqlGetWebinar = string.Format("select * from {0} where [CampaignID] = '{1}'", "vCampaignWebinarSummary", id);
            var webinarDt = conn.Get_DataTable(CommandType.Text, sqlGetWebinar, null);
            webinarDt.TableName = "Webinar_Detail";

            // Email
            var sqlGetEmail = string.Format("select * from {0} where [CampaignID] = '{1}'", "vCampaignEmailSummary", id);
            var emailDt = conn.Get_DataTable(CommandType.Text, sqlGetEmail, null);
            emailDt.TableName = "Email_Detail";

            var ds = new DataSet();
            ds.Tables.Add(summaryDt);
            ds.Tables.Add(webinarDt);
            ds.Tables.Add(emailDt);

            // add a cookie with the name 'dlc' and the value from the postback
            Response.Cookies.Add(new HttpCookie("dlc", time));

            ExcelHelper.ToExcel(ds, filename, Response);
        }

        public void ExportChannel(string id, string time)
        {
            vCampaignWebinarDetail webinar = db.vCampaignWebinarDetails.Where(w => w.MeetingID.Equals(id)).FirstOrDefault();
            var channel = webinar?.Channel;
            var topic = webinar?.Topic;
            var date = webinar?.Event_Date.Value.ToString("yyyy-mm-dd");

            if (string.IsNullOrEmpty(channel))
            {
                vCampaignEmailDetail email = db.vCampaignEmailDetails.Where(w => w.MeetingID.Equals(id)).FirstOrDefault();
                channel = email?.Channel;
                topic = email?.Topic;
                date = email?.Event_Date?.ToString("yyyy-mm-dd");
            }

            if (string.IsNullOrEmpty(channel))
                return;

            //var filename = "ChannelReport-" + id + "-" + DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss");
            var filename = string.Format("{0} - {1} - {2} - {3}", channel, id, topic, date);

            IChannelDownloader downloader;
            switch (channel)
            {
                case "Sanofi Zoom":
                    downloader = new SanofiZoomDownloader();
                    break;
                case "Republic M":
                    downloader = new RepublicMDownloader();
                    break;
                case "PigeonHole":
                    downloader = new PigeonHoleDownloader();
                    break;
                case "MailChimp":
                    downloader = new MailChimpDownloader();
                    break;
                case "Adobe Campaign":
                    downloader = new AdobeCampaignDownloader();
                    break;
                default:
                    return;
            }

            // add a cookie with the name 'dlc' and the value from the postback
            Response.Cookies.Add(new HttpCookie("dlc", time));

            var ds = downloader.GetDataSet(id);
            ExcelHelper.ToExcel(ds, filename, Response);
        }

        //generate campaignID
        public string GenerateCampaignID()
        {

            var yy = DateTime.Now.ToString("yy");
            var mm = DateTime.Now.ToString("MM");

            var cpFilter = "CP" + yy + mm;

            var LastID = db.Campaign_Header.Where(w => w.CampaignID.Contains(cpFilter)).OrderByDescending(o => o.CampaignID).Select(s => s.CampaignID).FirstOrDefault();
            var campaignID = cpFilter;
            if (LastID != null)
            {
                campaignID = campaignID + (Int32.Parse(LastID.Substring(7)) + 1).ToString("D5");
            }
            else
            {
                campaignID = campaignID + 1.ToString("D5");
            }
            return campaignID;
        }

        //create first campaign / not created
        public ActionResult GetCampaignID()
        {

            Campaign_Header campaignHeader = new Campaign_Header();

            //campaignHeader.CampaignID = GenerateCampaignID();
            campaignHeader.Status = ((int)CampaignStatus.NotCreated).ToString();
            campaignHeader.Date_Updated = DateTime.Now;
            db.Campaign_Header.Add(campaignHeader);
            db.SaveChanges();


            return RedirectToAction("Create");
        }



        public ActionResult Assign(string id)
        {
            if (id == "")
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }

            CampaignModel cm = new CampaignModel();

            var campaignHeader = db.Campaign_Header.Where(w => w.CampaignID == id && !w.Status.Equals(((int)CampaignStatus.Deleted).ToString())).FirstOrDefault();

            if (campaignHeader == null)
            {
                return HttpNotFound();
            }

            db.Configuration.LazyLoadingEnabled = false; // if your table is relational, contain foreign key
            db.Configuration.ProxyCreationEnabled = false;
            List<DTWebinarChannel> webinars = db.vCampaignWebinarDetails.Where(w => w.CampaignID.Equals(id)).Select(s => new DTWebinarChannel
            {
                WebinarID = s.MeetingID,
                Topic = s.Topic,
                EventDate = s.Event_Date,
                Source = s.Source,
            }).AsEnumerable().ToList();


            List<DTWebinarChannel> emails = db.vCampaignEmailDetails.Where(w => w.CampaignID.Equals(id)).Select(s => new DTWebinarChannel
            {
                WebinarID = s.MeetingID,
                Topic = s.Topic,
                EventDate = s.Event_Date,
                Source = s.Source,
            }).AsEnumerable().ToList();

            if (campaignHeader == null)
            {
                return HttpNotFound();
            }
            //cm.ID = campaignHeader.ID;
            cm.CampaignID = campaignHeader.CampaignID;
            cm.CampaignName = campaignHeader.CampaignName;
            cm.StartDate = campaignHeader.StartDate;
            cm.EndDate = campaignHeader.EndDate;
            cm.Description = campaignHeader.CampaignDescription;
            cm.Webinars = webinars;
            cm.Emails = emails;

            GenerateDDL(cm.CampaignID);

            return View(cm);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Assign(CampaignModel cm)
        {
            GenerateDDL(cm.CampaignID);
            List<DTWebinarChannel> webinars = db.vCampaignWebinarDetails.Where(w => w.CampaignID.Equals(cm.CampaignID)).Select(s => new DTWebinarChannel
            {
                WebinarID = s.MeetingID,
                Topic = s.Topic,
                EventDate = s.Event_Date,
                Source = s.Source,
            }).AsEnumerable().ToList();


            List<DTWebinarChannel> emails = db.vCampaignEmailDetails.Where(w => w.CampaignID.Equals(cm.CampaignID)).Select(s => new DTWebinarChannel
            {
                WebinarID = s.MeetingID,
                Topic = s.Topic,
                EventDate = s.Event_Date,
                Source = s.Source,
            }).AsEnumerable().ToList();

            cm.Webinars = webinars;
            cm.Emails = emails;

            if (ModelState.IsValid)
            {
                using (var dbTran = db.Database.BeginTransaction())
                {

                    try
                    {
                        Campaign_Header campaign_Header = new Campaign_Header();
                        campaign_Header = db.Campaign_Header.Where(w => w.CampaignID == cm.CampaignID).FirstOrDefault();
                        //campaign_Header.CampaignID = GenerateCampaignID();
                        campaign_Header.CampaignName = cm.CampaignName;
                        campaign_Header.BUID = Int32.Parse(cm.BusinessUnitID);
                        campaign_Header.CampaignDescription = cm.Description;
                        campaign_Header.StartDate = cm.StartDate;
                        campaign_Header.EndDate = cm.EndDate;
                        campaign_Header.Status = CheckCampaignDetail(cm.CampaignID) == true ? "2" : "1";
                        campaign_Header.Date_Updated = DateTime.Now;
                        campaign_Header.User_Updated = Session["UserName"].ToString();

                        db.Entry(campaign_Header).State = EntityState.Modified;
                        db.SaveChanges();
                        
                        var deleteCampaignBrand = "delete from Campaign_Brand where CampaignID = '" + cm.CampaignID + "'";
                        db.Database.ExecuteSqlCommand(deleteCampaignBrand);
                        foreach (var brand in cm.BrandID)
                        {
                            if (CheckBrandIsExist(cm.CampaignID, brand))
                            {
                                continue;
                            }
                            Campaign_Brand cb = new Campaign_Brand();
                            cb.CampaignID = cm.CampaignID;
                            cb.BrandID = brand;
                            cb.Status = ((int)Status.Active).ToString();
                            cb.Date_Updated = DateTime.Now;
                            cb.User_Updated = Session["UserName"].ToString();
                            db.Campaign_Brand.Add(cb);
                        }
                        db.SaveChanges();
                        
                        var deleteCampaignTargetAudience = "delete from Campaign_TargetAudience where CampaignID = '" + cm.CampaignID + "'";
                        db.Database.ExecuteSqlCommand(deleteCampaignTargetAudience);
                        foreach (var taID in cm.TargetAudienceID)
                        {
                            if (CheckTargetAudIsExist(cm.CampaignID, taID))
                            {
                                continue;
                            }

                            Campaign_TargetAudience ta = new Campaign_TargetAudience();
                            ta.CampaignID = cm.CampaignID;
                            ta.TargetAudienceID = taID;
                            ta.Status = ((int)Status.Active).ToString();
                            ta.Date_Updated = DateTime.Now;
                            ta.User_Updated = Session["UserName"].ToString();
                            db.Campaign_TargetAudience.Add(ta);
                        }
                        db.SaveChanges();

                        if (CheckCampaignDetail(cm.CampaignID))
                        {
                            var campaingDetailList = db.Campaign_Detail.Where(w => w.CampaignID == cm.CampaignID).ToList();
                            foreach (var cD in campaingDetailList)
                            {
                                //check periode if false delete campaign detail
                                var channelType = db.Mst_Channel.Where(w => w.ChannelID == cD.ChannelID).Select(s => s.ChannelType.ToLower()).FirstOrDefault();
                                switch (channelType)
                                {
                                    case "webinar":
                                        if (!CheckWebinarChannelPeriode(campaign_Header, cD.MeetingID))
                                        {
                                            db.Campaign_Detail.Remove(cD);
                                            db.SaveChanges();
                                        }
                                        break;
                                    case "email":
                                        if (!CheckEmailChannelPeriode(campaign_Header, cD.MeetingID))
                                        {
                                            db.Campaign_Detail.Remove(cD);
                                            db.SaveChanges();
                                        }
                                        break;
                                }
                            }
                        }

                        dbTran.Commit();
                    }
                    catch (Exception ex)
                    {
                        ViewBag.Error = ex.Message;
                        LogSystem.ErrorLog(ex, null);
                        dbTran.Rollback();
                        GenerateDDL(cm.CampaignID);
                        
                        return View(cm);
                    }
                }
              
                return RedirectToAction("Assign", "Campaign", new { id= cm.CampaignID });
            }
           
            

            return View(cm);
        }

        [HttpGet]
        public JsonResult LoadWebinarCampaign(string id)
        {
            try
            {
                db.Configuration.LazyLoadingEnabled = false; // if your table is relational, contain foreign key
                db.Configuration.ProxyCreationEnabled = false;
                // var data = db.vCampaignWebinarDetails.Where(w=>w.CampaignID==id).ToList();
                var qtable = " Select CampaignID, MeetingID, BU, Channel, Topic, ChannelID from [vCampaignWebinarDetail] where CampaignID='" + id + "'";
                var data = db.Database.SqlQuery<WebinarCampaignView>(qtable).ToList();

                return Json(new { status = true, data = data }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                LogSystem.ErrorLog(ex, null);
                return Json(new { status = false, dateToday = "", data = "" }, JsonRequestBehavior.AllowGet);
            }
        }

        [HttpGet]
        public JsonResult LoadEmailCampaign(string id)
        {
            try
            {
                db.Configuration.LazyLoadingEnabled = false; // if your table is relational, contain foreign key
                db.Configuration.ProxyCreationEnabled = false;
                //var data = db.vCampaignEmailDetails.Where(w => w.CampaignID == id).ToList();
                var qtable = " Select CampaignID, MeetingID, BU, Channel, Topic, ChannelID from [vCampaignEmailDetail] where CampaignID='" + id + "'";
                var data = db.Database.SqlQuery<WebinarCampaignView>(qtable).ToList();


                return Json(new { status = true, data = data }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                LogSystem.ErrorLog(ex, null);
                return Json(new { status = false, dateToday = "", data = "" }, JsonRequestBehavior.AllowGet);
            }
        }

        [HttpPost]
        public JsonResult LoadWebinarDataTableV2(string id)
        {
            try
            {
                var webinarChannelIDs = db.Mst_Channel.Where(w => w.ChannelType == "Webinar").Select(s => s.ChannelID).ToList();
                var campaignHeader = db.Campaign_Header.Where(w => w.CampaignID == id).FirstOrDefault();

                var selectedChannel = db.Campaign_Detail
                                                        .Where(w => w.CampaignID == id && webinarChannelIDs.Contains(w.ChannelID.Value))
                                                        .Select(s => s.MeetingID).ToList();

                var selectedChannelOtherCampaign = db.vCampaignWebinarDetails
                                                        .Where(w => w.CampaignID != id && webinarChannelIDs.Contains(w.ChannelID.Value) && !w.Status.Equals(((int)CampaignStatus.Deleted).ToString()))
                                                        .Select(s => s.MeetingID).ToList();

                List<DTWebinarChannel> vtrx = db.vChannelWebinarHeaders
                                                    .Where(w => w.Start_Date <= campaignHeader.EndDate && w.End_Date >= campaignHeader.StartDate && !selectedChannelOtherCampaign.Contains(w.WebinarID))
                                                    .Select(s => new DTWebinarChannel
                                                    {
                                                        isChecked = selectedChannel.Contains(s.WebinarID) ? true : false,
                                                        WebinarID = s.WebinarID,
                                                        Topic = s.Topic,
                                                        EventDate = s.Start_Date,
                                                        Source = s.Source
                                                    }).AsEnumerable().ToList();

                db.Configuration.LazyLoadingEnabled = false; // if your table is relational, contain foreign key
                db.Configuration.ProxyCreationEnabled = false;

                return Json(new { status = true, data = vtrx }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {

                LogSystem.ErrorLog(ex, null);
                return Json(new { status = false, dateToday = "", data = "" }, JsonRequestBehavior.AllowGet);
            }
        }


        [HttpPost]
        public JsonResult LoadWebinarChannel(string id)
        {
            try
            {
                ViewBag.Error = "";
                var draw = Request.Form.GetValues("draw").FirstOrDefault();
                var start = Request.Form.GetValues("start").FirstOrDefault();
                var length = Request.Form.GetValues("length").FirstOrDefault();

                //Find Order Column
                var sortColumn = Request.Form.GetValues("columns[" + Request.Form.GetValues("order[0][column]").FirstOrDefault() + "][name]").FirstOrDefault();
                var sortColumnDir = Request.Form.GetValues("order[0][dir]").FirstOrDefault();

                //Find Search Keyword
                var searchKey = Request.Form.GetValues("search[value]").FirstOrDefault();

                int pageSize = length != null ? Convert.ToInt32(length) : 0;
                int skip = start != null ? Convert.ToInt32(start) : 0;
                int recordsTotal = 0;
                var arrChannelWebinarID = db.Mst_Channel.Where(w => w.ChannelType == "Webinar").Select(s => s.ChannelID).ToList();
                var campaignHeader = db.Campaign_Header.Where(w => w.CampaignID == id).FirstOrDefault();
                List<Campaign_Detail> campDetail = db.Campaign_Detail.Where(w => w.CampaignID == id && arrChannelWebinarID.Contains(w.ChannelID.Value)).ToList();
                var arrMeetingIDDetail = campDetail.Select(s => s.MeetingID).ToList();
                db.Configuration.LazyLoadingEnabled = false; // if your table is relational, contain foreign key
                db.Configuration.ProxyCreationEnabled = false;
                List<DTWebinarChannel> vtrx = db.vChannelWebinarHeaders.Where(w => w.Start_Date <= campaignHeader.EndDate && w.End_Date >= campaignHeader.StartDate).Select(s => new DTWebinarChannel
                {
                    isChecked = arrMeetingIDDetail.Contains(s.WebinarID) ? true : false,
                    WebinarID = s.WebinarID,
                    Topic = s.Topic,
                    EventDate = s.Start_Date,
                    Source = s.Source
                }).AsEnumerable().ToList();

                recordsTotal = vtrx.Count();
                if (!string.IsNullOrEmpty(searchKey))
                {
                    vtrx = vtrx.Where(
                            w =>
                           w.WebinarID.ToLower().Contains(searchKey.ToLower())
                       || w.Topic.ToLower().Contains(searchKey.ToLower())
                       || w.Source.ToLower().Contains(searchKey.ToLower())
                        ).ToList();
                }

                //SORT
                if (!(string.IsNullOrEmpty(sortColumn) && string.IsNullOrEmpty(sortColumnDir)))
                {
                    var pi = typeof(DTWebinarChannel).GetProperty(sortColumn);
                    if (pi != null)
                    {
                        if (sortColumnDir == "asc")
                        {
                            vtrx = vtrx.OrderBy(x => pi.GetValue(x, null)).ToList();
                        }
                        else
                        {
                            vtrx = vtrx.OrderByDescending(x => pi.GetValue(x, null)).ToList();

                        }
                    }
                }

                var recordsFiltered = vtrx.Count();
                var data = vtrx.Skip(skip).Take(pageSize).ToList();
                string JSONString = JsonConvert.SerializeObject(data);
                var values = JsonConvert.DeserializeObject<List<Dictionary<string, string>>>(JSONString);

                return Json(new { draw = draw, recordsFiltered = recordsFiltered, recordsTotal = recordsTotal, data = values }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                LogSystem.ErrorLog(ex, null);
                return Json(new { status = false, dateToday = "", data = "" }, JsonRequestBehavior.AllowGet);
            }
        }

        [HttpPost]
        public JsonResult LoadEmailDataTableV2(string id)
        {
            try
            {
                var emailChannelIDs = db.Mst_Channel.Where(w => w.ChannelType == "Email").Select(s => s.ChannelID).ToList();
                var campaignHeader = db.Campaign_Header.Where(w => w.CampaignID == id).FirstOrDefault();

                var selectedChannel = db.Campaign_Detail
                                                        .Where(w => w.CampaignID == id && emailChannelIDs.Contains(w.ChannelID.Value))
                                                        .Select(s => s.MeetingID).ToList();

                var selectedChannelOtherCampaign = db.vCampaignEmailDetails
                                                        .Where(w => w.CampaignID != id && emailChannelIDs.Contains(w.ChannelID.Value) && !w.Status.Equals(((int)CampaignStatus.Deleted).ToString()))
                                                        .Select(s => s.MeetingID).ToList();

                List<DTWebinarChannel> vtrx = db.vChannelEmailHeaders
                                                    .Where(w => (w.Start_Date == null || (w.Start_Date >= campaignHeader.StartDate && w.Start_Date <= campaignHeader.EndDate)) && !selectedChannelOtherCampaign.Contains(w.MeetingID))
                                                    .Select(s => new DTWebinarChannel
                                                    {
                                                        isChecked = selectedChannel.Contains(s.MeetingID) ? true : false,
                                                        WebinarID = s.MeetingID,
                                                        Topic = s.Topic,
                                                        EventDate = s.Start_Date,
                                                        Source = s.Source,
                                                    }).AsEnumerable().ToList();

                db.Configuration.LazyLoadingEnabled = false; // if your table is relational, contain foreign key
                db.Configuration.ProxyCreationEnabled = false;

                return Json(new { status = true, data = vtrx }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {

                LogSystem.ErrorLog(ex, null);
                return Json(new { status = false, dateToday = "", data = "" }, JsonRequestBehavior.AllowGet);
            }
        }

        [HttpPost]
        public JsonResult LoadEmailChannel(string id)
        {
            try
            {
                ViewBag.Error = "";
                var draw = Request.Form.GetValues("draw").FirstOrDefault();
                var start = Request.Form.GetValues("start").FirstOrDefault();
                var length = Request.Form.GetValues("length").FirstOrDefault();

                //Find Order Column
                var sortColumn = Request.Form.GetValues("columns[" + Request.Form.GetValues("order[0][column]").FirstOrDefault() + "][name]").FirstOrDefault();
                var sortColumnDir = Request.Form.GetValues("order[0][dir]").FirstOrDefault();

                //Find Search Keyword
                var searchKey = Request.Form.GetValues("search[value]").FirstOrDefault();

                int pageSize = length != null ? Convert.ToInt32(length) : 0;
                int skip = start != null ? Convert.ToInt32(start) : 0;
                int recordsTotal = 0;
                var campaignHeader = db.Campaign_Header.Where(w => w.CampaignID == id).FirstOrDefault();
                db.Configuration.LazyLoadingEnabled = false; // if your table is relational, contain foreign key
                db.Configuration.ProxyCreationEnabled = false;
                var arrChannelEmailID = db.Mst_Channel.Where(w => w.ChannelType == "Email").Select(s => s.ChannelID).ToList();
                List<Campaign_Detail> campDetail = db.Campaign_Detail.Where(w => w.CampaignID == id && arrChannelEmailID.Contains(w.ChannelID.Value)).ToList();
                var arrMeetingIDDetail = campDetail.Select(s => s.MeetingID).ToList();
                List<DTWebinarChannel> vtrx = db.vChannelEmailHeaders.Where(w => w.Start_Date == null || (w.Start_Date >= campaignHeader.StartDate && w.Start_Date <= campaignHeader.EndDate)).Select(s => new DTWebinarChannel
                {
                    isChecked = arrMeetingIDDetail.Contains(s.MeetingID) ? true : false,
                    WebinarID = s.MeetingID,
                    Topic = s.Topic,
                    EventDate = s.Start_Date,
                    Source = s.Source,
                }).AsEnumerable().ToList();

                recordsTotal = vtrx.Count();
                if (!string.IsNullOrEmpty(searchKey))
                {
                    vtrx = vtrx.Where(
                            w =>
                           w.WebinarID.ToLower().Contains(searchKey.ToLower())
                       || w.Topic.ToLower().Contains(searchKey.ToLower())
                       || w.Source.ToLower().Contains(searchKey.ToLower())
                        ).ToList();
                }

                //SORT
                if (!(string.IsNullOrEmpty(sortColumn) && string.IsNullOrEmpty(sortColumnDir)))
                {
                    //sortColumn = sortColumn == "w.reg" ? "trxLoan.assetName" : sortColumn;
                    var pi = typeof(DTWebinarChannel).GetProperty(sortColumn);
                    if (pi != null)
                    {
                        if (sortColumnDir == "asc")
                        {
                            vtrx = vtrx.OrderBy(x => pi.GetValue(x, null)).ToList();
                        }
                        else
                        {
                            vtrx = vtrx.OrderByDescending(x => pi.GetValue(x, null)).ToList();

                        }
                    }
                }

                var recordsFiltered = vtrx.Count();
                var data = vtrx.Skip(skip).Take(pageSize).ToList();
                string JSONString = JsonConvert.SerializeObject(data);
                var values = JsonConvert.DeserializeObject<List<Dictionary<string, string>>>(JSONString);

                return Json(new { draw = draw, recordsFiltered = recordsFiltered, recordsTotal = recordsTotal, data = values }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                LogSystem.ErrorLog(ex, null);
                return Json(new { status = false, dateToday = "", data = "" }, JsonRequestBehavior.AllowGet);
            }
        }

        //[HttpGet]
        //public JsonResult LoadEmailChannel(string campaignID)
        //{
        //    try
        //    {
        //        var data = db.vChannelEmailHeaders.ToList();
        //        db.Configuration.LazyLoadingEnabled = false; // if your table is relational, contain foreign key
        //        db.Configuration.ProxyCreationEnabled = false;
        //        return Json(new { status = true, data = data }, JsonRequestBehavior.AllowGet);
        //    }
        //    catch (Exception ex)
        //    {
        //        LogSystem.ErrorLog(ex, null);
        //        return Json(new { status = false, dateToday = "", data = "" }, JsonRequestBehavior.AllowGet);
        //    }

        //}

        [HttpGet]
        public JsonResult GetSimiliarCampaignName(string name)
        {
            try
            {
                List<Campaign_Header> data = new List<Campaign_Header>();
                if (name != "")
                {
                    data = db.Campaign_Header.Where(w => w.CampaignName.Contains(name) && w.Status!=((int)CampaignStatus.Deleted).ToString() && w.Status != ((int)CampaignStatus.NotCreated).ToString() && w.Status!= ((int)CampaignStatus.NotActive).ToString()).ToList();
                }
                db.Configuration.LazyLoadingEnabled = false; // if your table is relational, contain foreign key
                db.Configuration.ProxyCreationEnabled = false;
                return Json(new { status = true, data = data }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                LogSystem.ErrorLog(ex, null);
                return Json(new { status = false, dateToday = "", data = "" }, JsonRequestBehavior.AllowGet);
            }

        }

        [HttpPost]
        public JsonResult CreateWebinarChannel(string[] ids, string[] ids2, string campaignId)
        {
            if (ModelState.IsValid)
            {
                using (var dbTran = db.Database.BeginTransaction())
                {

                    try
                    {
                        //delete detail
                        var QueryString = "delete from Campaign_Detail where CampaignID = '" + campaignId + "' AND ChannelID IN (select ChannelID from mst_channel where channelType = 'Webinar')";
                        var data = db.Database.ExecuteSqlCommand(QueryString);
                        if (ids != null)
                        {
                            foreach (var webID in ids)
                            {

                                //vChannelWebinarHeader vCW = new vChannelWebinarHeader();
                                var vCW = db.vChannelWebinarHeaders.Where(w => w.WebinarID == webID).Select(s => new
                                {
                                    MeetingID = s.WebinarID,
                                    ChannelID = s.ChannelID,
                                }).FirstOrDefault();

                                Campaign_Detail campaign_Detail = new Campaign_Detail();
                                campaign_Detail.CampaignID = campaignId;
                                campaign_Detail.MeetingID = vCW.MeetingID;
                                campaign_Detail.ChannelID = vCW.ChannelID;
                                campaign_Detail.Status = ((int)Status.Active).ToString();
                                campaign_Detail.Date_Updated = DateTime.Now;
                                campaign_Detail.User_Updated = Session["UserName"].ToString();
                                db.Campaign_Detail.Add(campaign_Detail);
                            }
                        }
                        db.SaveChanges();

                        UpdateStatusAssign(campaignId);

                        dbTran.Commit();
                        return Json(new { status = true, msg = "success" }, JsonRequestBehavior.AllowGet);
                    }

                    catch (Exception ex)
                    {
                        LogSystem.ErrorLog(ex, null);
                        dbTran.Rollback();
                        return Json(new { success = false, msg = ex }, JsonRequestBehavior.AllowGet);
                    }
                }
            }

            return Json(new { success = false, msg = "Something worng!" }, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public JsonResult CreateEmailChannel(string[] ids, string campaignId)
        {
            if (ModelState.IsValid)
            {
                using (var dbTran = db.Database.BeginTransaction())
                {

                    try
                    {
                        var QueryString = "delete from Campaign_Detail where CampaignID = '" + campaignId + "' AND ChannelID IN (select ChannelID from mst_channel where channelType = 'Email')";
                        var data = db.Database.ExecuteSqlCommand(QueryString);
                        if (ids != null)
                        {
                            foreach (var meetID in ids)
                            {

                                //vChannelEmailHeader vCM = new vChannelEmailHeader();
                                var vCM = db.vChannelEmailHeaders.Where(w => w.MeetingID == meetID).Select(s => new
                                {
                                    MeetingID = s.MeetingID,
                                    ChannelID = s.ChannelID,
                                }).FirstOrDefault();

                                Campaign_Detail campaign_Detail = new Campaign_Detail();
                                campaign_Detail.CampaignID = campaignId;
                                campaign_Detail.MeetingID = vCM.MeetingID;
                                campaign_Detail.ChannelID = vCM.ChannelID;
                                campaign_Detail.Status = ((int)Status.Active).ToString();
                                campaign_Detail.Date_Updated = DateTime.Now;
                                campaign_Detail.User_Updated = Session["UserName"].ToString();
                                db.Campaign_Detail.Add(campaign_Detail);
                            }
                        }
                        db.SaveChanges();
                        dbTran.Commit();
                        UpdateStatusAssign(campaignId);
                        return Json(new { status = true, msg = "success" }, JsonRequestBehavior.AllowGet);
                    }

                    catch (Exception ex)
                    {
                        LogSystem.ErrorLog(ex, null);
                        dbTran.Rollback();
                        return Json(new { success = false, msg = ex }, JsonRequestBehavior.AllowGet);
                    }
                }
            }

            return Json(new { success = false, msg = "Something worng!" }, JsonRequestBehavior.AllowGet);
        }

        //delete campaign cancel when create first
        [HttpPost]
        public JsonResult CancelCampaign()
        {
            if (ModelState.IsValid)
            {
                using (var dbTran = db.Database.BeginTransaction())
                {

                    try
                    {

                        List<Campaign_Header> cH = new List<Campaign_Header>();
                        cH = db.Campaign_Header.Where(w => w.CampaignID == "" || w.CampaignID == null).Where(w => w.Status == ((int)CampaignStatus.NotCreated).ToString()).ToList();

                        if (cH.Count >= 0)
                        {
                            db.Campaign_Header.RemoveRange(cH);
                            db.SaveChanges();
                        }

                        dbTran.Commit();
                        return Json(new { status = true, msg = "success" }, JsonRequestBehavior.AllowGet);

                    }

                    catch (Exception ex)
                    {
                        LogSystem.ErrorLog(ex, null);
                        dbTran.Rollback();
                        return Json(new { success = false, msg = ex }, JsonRequestBehavior.AllowGet);
                    }
                }
            }

            return Json(new { success = false, msg = "Something worng!" }, JsonRequestBehavior.AllowGet);
        }

        //duplicate campaign
        //[HttpPost]
        public ActionResult DuplicateCampaign(string id)
        {
            //using (var dbTran = db.Database.BeginTransaction())
            //{

            try
            {
                CampaignModel newDuplicateCampaign = new CampaignModel();
                var cH = db.Campaign_Header.Where(w => w.CampaignID == id).FirstOrDefault();


                //newDuplicateCampaign.CampaignID = GenerateCampaignID();
                //WIP

                var increamentName = getStringBetween(cH.CampaignName, "(", ")");
                if (increamentName == "")
                {
                    increamentName = cH.CampaignName + " (1)";
                }
                else
                {
                    int index = cH.CampaignName.IndexOf("(");
                    var originName = cH.CampaignName.Remove(index);
                    var cHName = db.Campaign_Header.Where(w => w.CampaignName.Contains(originName)).OrderByDescending(o => o.CampaignName).FirstOrDefault();
                    var inc = Int32.Parse(increamentName) + 1;
                    increamentName = originName + " (" + inc + ")";
                }
                newDuplicateCampaign.CampaignName = increamentName;
                newDuplicateCampaign.Description = cH.CampaignDescription;
                newDuplicateCampaign.StartDate = cH.StartDate;
                newDuplicateCampaign.EndDate = cH.EndDate;
                //newDuplicateCampaign.Date_Updated = DateTime.Now;
                //newDuplicateCampaign.User_Updated = Session["UserName"].ToString();
                //newDuplicateCampaign.Status = ((int)CampaignStatus.NotCreated).ToString();

                //db.Campaign_Header.Add(newDuplicateCampaign);

                //db.SaveChanges();
                //dbTran.Commit();
                //return Json(new { status = true, msg = "success", data = newDuplicateCampaign.CampaignID }, JsonRequestBehavior.AllowGet);


                GenerateDDL(id);

                return View("Create", newDuplicateCampaign);
            }

            catch (Exception ex)
            {
                LogSystem.ErrorLog(ex, null);
                //dbTran.Rollback();
                return Json(new { success = false, msg = ex, data = "" }, JsonRequestBehavior.AllowGet);
            }
            //}
        }

        [HttpGet]
        public JsonResult LoadCampaign(string campaignID)
        {
            try
            {
                var data = db.Campaign_TargetAudience.Where(w => w.CampaignID == campaignID).ToList();

                return Json(new { status = true, data = data }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                LogSystem.ErrorLog(ex, null);
                return Json(new { status = false, dateToday = "", data = "" }, JsonRequestBehavior.AllowGet);
            }

        }

        //update status campaign to assign 
        private void UpdateStatusAssign(string campaignID)
        {
            Campaign_Header campaign_Header = new Campaign_Header();
            campaign_Header = db.Campaign_Header.Where(w => w.CampaignID == campaignID).FirstOrDefault();
            campaign_Header.Status = ((int)CampaignStatus.Assigned).ToString();
            db.Entry(campaign_Header).State = EntityState.Modified;
            db.SaveChanges();
        }

        //CheckBrandIsExist
        private bool CheckBrandIsExist(string campaignID, int BrandID)
        {
            var brand = db.Campaign_Brand.Where(w => w.CampaignID == campaignID && w.BrandID == BrandID).FirstOrDefault();
            if (brand == null)
            {
                return false;
            }
            return true;
        }

        //CheckTargetAudIsExist
        private bool CheckTargetAudIsExist(string campaignID, int TargetAudID)
        {
            var TargetAudience = db.Campaign_TargetAudience.Where(w => w.CampaignID == campaignID && w.TargetAudienceID == TargetAudID).FirstOrDefault();
            if (TargetAudience == null)
            {
                return false;
            }
            return true;
        }

        //CheckMeetIDCampaignIsExist
        private bool CheckMeetIDCampaignIsExist(string meetID, string campaignId)
        {
            var campaignDetail = db.Campaign_Detail.Where(w => w.MeetingID == meetID && w.CampaignID == campaignId).FirstOrDefault();
            if (campaignDetail == null)
            {
                return false;
            }
            return true;
        }

        //Delete Webinar Campaign Detail
        private bool DeleteWebinarCampaignDetail(string campaignId)
        {
            try
            {
                var QueryString = "delete from Campaign_Detail where CampaignID = 'id ne' AND ChannelID IN (select ChannelID from mst_channel where channelType = 'Webinar')";
                var data = db.Database.ExecuteSqlCommand(QueryString);
            }
            catch (Exception)
            {

                return false;
            }

            return true;
        }

        //Delete Email Campaign Detail
        private bool DeleteEmailCampaignDetail(string campaignId)
        {
            try
            {
                var QueryString = "delete from Campaign_Detail where CampaignID = 'id ne' AND ChannelID IN (select ChannelID from mst_channel where channelType = 'Email')";
                var data = db.Database.ExecuteSqlCommand(QueryString);
            }
            catch (Exception)
            {

                return false;
            }

            return true;
        }

        //CheckwebinarChannel IN PERIODE
        private bool CheckWebinarChannelPeriode(Campaign_Header campHeader, string meetingID)
        {
            //var webinarChannel = db.vChannelWebinarHeaders.Where(w => w.WebinarID == meetingID).FirstOrDefault();
            var qtable = " Select CampaignID, MeetingID, BU, Channel, Topic, ChannelID, Event_Date from [vCampaignWebinarDetail] where MeetingID='" + meetingID + "'";
            var data = db.Database.SqlQuery<WebinarCampaignView>(qtable).FirstOrDefault();

            if (!data.Event_Date.HasValue) return true;

            if (data.Event_Date.Value.Date >= campHeader.StartDate.Value.Date && data.Event_Date.Value.Date <= campHeader.EndDate.Value.Date)
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        //CheckwebinarChannel IN PERIODE
        private bool CheckEmailChannelPeriode(Campaign_Header campHeader, string meetingID)
        {
            //var emailChannel = db.vChannelEmailHeaders.Where(w => w.MeetingID == meetingID).FirstOrDefault();
            var qtable = " Select CampaignID, MeetingID, BU, Channel, Topic, ChannelID, Event_Date from [vCampaignEmailDetail] where MeetingID='" + meetingID + "'";
            var data = db.Database.SqlQuery<WebinarCampaignView>(qtable).FirstOrDefault();
            
            if (!data.Event_Date.HasValue) return true;

            if (data.Event_Date.Value.Date >= campHeader.StartDate.Value.Date && data.Event_Date.Value.Date <= campHeader.EndDate.Value.Date)
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        //CheckCampaignDetail
        private bool CheckCampaignDetail(string campaignD)
        {
            var campaignDetail = db.Campaign_Detail.Where(w => w.CampaignID == campaignD).FirstOrDefault();
            if (campaignDetail == null)
            {
                return false;
            }
            return true;
        }

        //get string between
        private string getStringBetween(string strSource, string strStart, string strEnd)
        {
            if (strSource.Contains(strStart) && strSource.Contains(strEnd))
            {
                int Start, End;
                Start = strSource.IndexOf(strStart, 0) + strStart.Length;
                End = strSource.IndexOf(strEnd, Start);
                return strSource.Substring(Start, End - Start);
            }

            return "";
        }

        [HttpPost]
        public JsonResult Delete(string id)
        {
            using (var dbTran = db.Database.BeginTransaction())
            {
                try
                {
                    Campaign_Header newDuplicateCampaign = new Campaign_Header();
                    var cH = db.Campaign_Header.Where(w => w.CampaignID == id).FirstOrDefault();


                    cH.Status = ((int)CampaignStatus.Deleted).ToString();


                    db.SaveChanges();
                    dbTran.Commit();
                    return Json(new { status = true, msg = "success" }, JsonRequestBehavior.AllowGet);

                }

                catch (Exception ex)
                {
                    LogSystem.ErrorLog(ex, null);
                    dbTran.Rollback();
                    return Json(new { success = false, msg = ex, data = "" }, JsonRequestBehavior.AllowGet);
                }
            }
        }
    }

}