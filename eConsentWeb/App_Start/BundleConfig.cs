﻿using System.Web;
using System.Web.Optimization;

namespace eConsentWeb
{
    public class BundleConfig
    {
        // For more information on bundling, visit http://go.microsoft.com/fwlink/?LinkId=301862
        public static void RegisterBundles(BundleCollection bundles)
        {
            // CS
            bundles.Add(new StyleBundle("~/CSS/main").Include(
                    "~/Assets/global/css/open-sans.css",
                    "~/Assets/global/plugins/font-awesome/css/font-awesome.min.css",
                    "~/Assets/global/plugins/simple-line-icons/simple-line-icons.min.css",
                    "~/Assets/global/plugins/bootstrap/css/bootstrap.min.css"
            ));

            bundles.Add(new StyleBundle("~/CSS/layout").Include(
                    "~/Assets/global/css/components-rounded.min.css",
                    "~/Assets/global/css/plugins.min.css",
                    "~/Assets/global/css/spacing.css",
                    "~/Assets/global/css/utils.css",
                    "~/Assets/layouts/layout4/css/layout.min.css",
                    "~/Assets/layouts/layout4/css/themes/default.min.css",
                    "~/Assets/layouts/layout4/css/custom.min.css"
            ));

            bundles.Add(new StyleBundle("~/CSS/components").Include(
                    "~/Assets/global/plugins/bootstrap-switch/css/bootstrap-switch.min.css",
                    "~/Assets/global/plugins/select2/css/select2.min.css",
                    "~/Assets/global/plugins/select2/css/select2-bootstrap.min.css",
                    "~/Assets/global/plugins/icheck/skins/all.css",
                    "~/Assets/global/plugins/datatables/datatables.min.css",
                    "~/Assets/global/plugins/datatables/plugins/bootstrap/datatables.bootstrap.css",
                    "~/Assets/global/plugins/bootstrap-sweetalert/sweetalert.css",
                    "~/Assets/global/plugins/bootstrap-summernote/summernote.css",
                    "~/Assets/global/plugins/bootstrap-datepicker/css/bootstrap-datepicker3.min.css",
                    "~/Assets/global/plugins/bootstrap-timepicker/css/bootstrap-timepicker.min.css",
                    "~/Assets/global/plugins/bootstrap-select/css/bootstrap-select.css",
                    "~/Assets/global/plugins/fullcalendar/fullcalendar.min.css",
                    "~/Assets/global/plugins/jquery-rating/bars-pill.css",
                    "~/Assets/global/plugins/jquery-rating/fontawesome-stars.css",
                    "~/Assets/global/plugins/bootstrap-tagsinput/bootstrap-tagsinput.css"
            ));

            // Javascript
            bundles.Add(new ScriptBundle("~/JS/main").Include(
                    "~/Assets/global/plugins/jquery.min.js",
                    "~/Assets/global/plugins/bootstrap/js/bootstrap.min.js",
                    "~/Assets/global/plugins/js.cookie.min.js"
            ));

            bundles.Add(new ScriptBundle("~/JS/components").Include(
                    "~/Assets/global/plugins/jquery.blockui.min.js",
                    "~/Assets/global/plugins/bootstrap-switch/js/bootstrap-switch.min.js",
                    "~/Assets/global/plugins/select2/js/select2.full.min.js",
                    "~/Assets/global/plugins/jquery-slimscroll/jquery.slimscroll.min.js",
                    "~/Assets/global/scripts/datatable.js",
                    "~/Assets/global/plugins/datatables/datatables.min.js",
                    "~/Assets/global/plugins/datatables/plugins/bootstrap/datatables.bootstrap.js",
                    "~/Assets/global/plugins/bootstrap-datepicker/js/bootstrap-datepicker.min.js",
                    "~/Assets/global/plugins/jquery-repeater/jquery.repeater.js",
                    "~/Assets/global/plugins/bootstrap-sweetalert/sweetalert.min.js",
                    "~/Assets/global/plugins/icheck/icheck.js",
                    "~/Assets/global/scripts/app.js"
            ));

            bundles.Add(new ScriptBundle("~/JS/layout").Include(
                    "~/Assets/layouts/layout4/scripts/layout.js",
                    "~/Assets/layouts/global/scripts/quick-nav.js",
                    "~/Assets/layouts/global/scripts/quick-sidebar.js",
                    "~/Assets/global/scripts/general.js"
            ));

            bundles.Add(new ScriptBundle("~/JS/jqueryval").Include(
                    "~/Scripts/jquery.validate.min.js",
                    "~/Scripts/jquery.validate.unobtrusive.min.js"
            ));

            BundleTable.EnableOptimizations = false;
        }
    }
}
