﻿using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;

namespace eConsentWeb.Common
{
    public static class JsonSettings
    {
        private static string fileName = "settings.json";
        private static string path = string.Format("{0}/{1}", HttpContext.Current.Server.MapPath("~/Content/"), fileName);

        public static string GetSetting(string key)
        {
            try
            {
                using (StreamReader r = new StreamReader(path))
                {
                    using(JsonTextReader reader = new JsonTextReader(r))
                    {
                        JObject o = (JObject)JToken.ReadFrom(reader);
                        
                        return o[key].ToString();
                    }
                }
            }
            catch (Exception ex)
            {
                LogSystem.ErrorLog(ex, null);
                return "";
            }
        }
    }
}