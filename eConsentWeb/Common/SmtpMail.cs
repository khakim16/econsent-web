﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Net.Mail;
using System.Text;
using System.Net;
using System.Configuration;

namespace ContractCenter.Common
{
    public class SmtpMail
    {
        public void SendMail(string to, string bcc, string cc, string subject, string body)
        {

            String email = ConfigurationManager.AppSettings["email"];
            String emailpass = ConfigurationManager.AppSettings["email_pass"];
            String smtpclient = ConfigurationManager.AppSettings["smtp_client"];
            Int32 emailport = Int32.Parse(ConfigurationManager.AppSettings["email_port"]);

            var _mail = new MailMessage(new MailAddress(email), new MailAddress(to))
            {
                Subject = subject,
                Body = body,
                BodyEncoding = Encoding.UTF8,
                IsBodyHtml = true,
                Priority = MailPriority.High
            };

            if (!string.IsNullOrWhiteSpace(cc))
            {
                _mail.CC.Add(cc);
            }

            if (!string.IsNullOrWhiteSpace(bcc))
            {
                _mail.Bcc.Add(bcc);
            }
            //SmtpClient smtpMail = new SmtpClient("smtp.gmail.com");
            //var _smtpMail = new SmtpClient("smtp.gmail.com", 587)
            var _smtpMail = new SmtpClient(smtpclient, emailport)
            {
                UseDefaultCredentials = false,
                DeliveryMethod = SmtpDeliveryMethod.Network,
                EnableSsl = true,
                Timeout = 30000,
                Credentials = new NetworkCredential(email, emailpass),
                //Credentials = new NetworkCredential("nanda.intellisys@gmail.com", "Intellisys9"),
            };
                        
            // and then send the mail
            _smtpMail.Send(_mail);
          

        }
    }
}