﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;

namespace eConsentWeb.Common
{
    public class SQLConnect
    {
        private string ConnStr = "";

        public SQLConnect(string cnStr)
        {
            ConnStr = cnStr;
        }

        public SQLConnect()
        {
            
        }

        public void InitCommand(SqlConnection conn, System.Data.SqlClient.SqlCommand cmd, CommandType cmdType, string cmdText, SqlParameter[] cmdParms)
        {
            if (conn.State != ConnectionState.Open) conn.Open();

            cmd.Connection = conn;
            cmd.CommandText = cmdText;
            cmd.CommandType = cmdType;

            if (cmdParms != null)
            {
                foreach (SqlParameter parm in cmdParms)
                    cmd.Parameters.Add(parm);
            }
        }

        public int ExecuteNonQuery(CommandType cmdType, string cmdText, params SqlParameter[] cmdParms)
        {
            SqlCommand cmd = new SqlCommand();
            try
            {
                using (SqlConnection conn = new SqlConnection(ConnStr))
                {
                    InitCommand(conn, cmd, cmdType, cmdText, cmdParms);

                    int val = cmd.ExecuteNonQuery();
                    cmd.Parameters.Clear();
                    return val;
                }
            }
            catch (System.Exception ex)
            {
                LogSystem.ErrorLog(ex, cmdText);
                return 0;
            }
        }

        public DataSet Get_DataSet(CommandType cmdType, string cmdText, params SqlParameter[] cmdParms)
        {
            SqlCommand cmd = new SqlCommand();
            try
            {
                using (SqlConnection conn = new SqlConnection(ConnStr))
                {
                    InitCommand(conn, cmd, cmdType, cmdText, cmdParms);
                    SqlDataAdapter sqlda = new SqlDataAdapter(cmd);
                    DataSet ds = new DataSet();
                    sqlda.Fill(ds);

                    cmd.Parameters.Clear();
                    return ds;
                }
            }
            catch (System.Exception ex)
            {
                LogSystem.ErrorLog(ex, cmdText);
                return null;
            }
        }

        public DataTable Get_DataTable(CommandType cmdType, string cmdText, params SqlParameter[] cmdParms)
        {
            SqlCommand cmd = new SqlCommand();
            cmd.CommandTimeout = 0;
            try
            {
                using (SqlConnection conn = new SqlConnection(ConnStr))
                {
                    InitCommand(conn, cmd, cmdType, cmdText, cmdParms);
                    SqlDataAdapter sqlda = new SqlDataAdapter(cmd);
                    DataTable dt = new DataTable();
                    sqlda.Fill(dt);
                    cmd.Parameters.Clear();
                    return dt;
                }
            }
            catch (System.Exception ex)
            {
                LogSystem.ErrorLog(ex, cmdText);
                return null;
            }
        }

        public string ExecuteScalar(CommandType cmdType, string cmdText, params SqlParameter[] cmdParms)
        {
            SqlCommand cmd = new SqlCommand();
            try
            {
                using (SqlConnection conn = new SqlConnection(ConnStr))
                {
                    InitCommand(conn, cmd, cmdType, cmdText, cmdParms);

                    string p_return = cmd.ExecuteScalar().ToString();
                    cmd.Parameters.Clear();
                    return p_return;
                }
            }
            catch (System.Exception ex)
            {
                LogSystem.ErrorLog(ex, cmdText);
                return null;
            }
        }
    }
}