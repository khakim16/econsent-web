﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;

namespace eConsentWeb.Common
{
    public class LogSystem
    {
        private static string folderPath = AppDomain.CurrentDomain.BaseDirectory + "/Log";
        //private static string fileName = "\\Log.txt";
        private static string errorPath = AppDomain.CurrentDomain.BaseDirectory + "/ErrorLog";
        private static string errorfileName = "\\ErrorLog.txt";
        
        public static void ErrorLog(System.Exception ex, string sqlCmd)
        {
            string text = ex.Message;
            string datetime = " Error at " + DateTime.Now.ToString("dd/MM/yyyy") + " " + DateTime.Now.ToString("HH:mm:ss") + ": ";
            
            string id = "#" + DateTime.Now.ToString("ddMMyyyy") + DateTime.Now.ToString("HHmmss");
            string message = id + " \r\nError at: " + DateTime.Now.ToString("dd/MM/yyyy") + " " + DateTime.Now.ToString("HH:mm:ss") +
                //"\r\nError in: " + Ur +
                "\r\nError Message: " + ex.Message.ToString() +
                "\r\nInner Exception: \r\n" + ex.ToString() +
                "\r\nStack Trace: \r\n" + ex.StackTrace.ToString();

            FileStream fs = new FileStream(errorPath + errorfileName, FileMode.OpenOrCreate, FileAccess.Write);
            StreamWriter m_streamWriter = new StreamWriter(fs);
            m_streamWriter.BaseStream.Seek(0, SeekOrigin.End);
            m_streamWriter.WriteLine(message + "\r\n");

            m_streamWriter.Flush();
            m_streamWriter.Close();
        }
    }
}