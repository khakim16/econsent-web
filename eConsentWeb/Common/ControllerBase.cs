﻿using eConsentWeb.Models;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace eConsentWeb.Common
{
    public class LogActionFilter : ActionFilterAttribute
    {
        public override void OnActionExecuting(ActionExecutingContext filterContext)
        {
            var routeData = filterContext.HttpContext.Request.RequestContext.RouteData;

            string controller = routeData.Values["controller"].ToString();
            string area = routeData.DataTokens.ContainsKey("area") ? routeData.DataTokens["area"].ToString() + "/" : "";
            string action = routeData.Values["action"].ToString();
            string pageName = area + controller + "/" + action;

            // TODO: unremark this line
            ControlAuth.CheckSession(pageName, filterContext);

            // TODO: unremark this line
            ControlAuth.CheckControl(area, controller, action, filterContext);
        }
    }

    [LogActionFilter]
    public class ParentController : Controller
    {
        public DataTablesViewModel GetParameter()
        {
            var result = new DataTablesViewModel();
            try
            {
                var dict = Request.Form.ToDictionary();

                string draw = null;
                string start = null;
                string length = null;
                string sortColumn = null;
                string sortColumnDir = null;
                string keyword = null;

                dict.TryGetValue("draw", out draw);
                dict.TryGetValue("start", out start);
                dict.TryGetValue("length", out length);
                dict.TryGetValue("columns[" + (dict.ContainsKey("order[0][column]") ? dict["order[0][column]"] : "") + "][data]", out sortColumn);
                dict.TryGetValue("order[0][dir]", out sortColumnDir);
                dict.TryGetValue("search[value]", out keyword);

                int iDraw    = draw != null ? Convert.ToInt32(draw) : 0;
                int skip     = start != null ? Convert.ToInt32(start) : 0;
                int pageSize = length != null ? Convert.ToInt32(length) : 5;

                result.draw          = iDraw;
                result.offset        = skip;
                result.size          = pageSize;
                result.sortColumn    = sortColumn;
                result.sortColumnDir = sortColumnDir;
                result.keyword       = keyword;
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex);
            }
            return result;
        }

        public void ExportToFile(object data, string filename)
        {
            var gv = new GridView();
            gv.DataSource = data;
            gv.DataBind();
            Response.ClearContent();
            Response.Buffer = true;
            Response.AddHeader("content-disposition", "attachment; filename=\"" + filename + ".xls\"");
            Response.ContentType = "application/ms-excel";
            Response.Charset = "";
            StringWriter objStringWriter = new StringWriter();
            HtmlTextWriter objHtmlTextWriter = new HtmlTextWriter(objStringWriter);
            gv.RenderControl(objHtmlTextWriter);
            Response.Output.Write(objStringWriter.ToString());

            Response.Flush();
            Response.End();
        }
    }
}